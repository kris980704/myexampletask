export const setData = payload => ({ type: "SET_DATA", payload });
export const searchData = payload => ({ type: "SEARCH_DATA", payload });