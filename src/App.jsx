import React, { Component } from "react";
import { connect } from 'react-redux';
import * as actions from './constants/actions';
import * as selectors from './constants/selectors';
import DenseTable from "./Table.jsx";

class App extends Component {
constructor(props) {
  super(props);
  fetch('https://news.umarkets.com/feed/filter/full?count=10&lang=ru&tags=%D1%80%D1%8B%D0%BD%D0%BA%D0%B8,%D0%BC%D0%B8%D1%80%D0%BE%D0%B2%D1%8B%D0%B5+%D1%80%D1%8B%D0%BD%D0%BA%D0%B8,%D0%BD%D0%B5%D1%84%D1%82%D1%8C,%D1%81%D1%8B%D1%80%D1%8C%D0%B5%D0%B2%D1%8B%D0%B5+%D1%80%D1%8B%D0%BD%D0%BA%D0%B8,%D0%BF%D0%BE%D1%82%D1%80%D0%B5%D0%B1%D0%B8%D1%82%D0%B5%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B9+%D1%80%D1%8B%D0%BD%D0%BE%D0%')
      .then(response => response.json())
      .then(data => props.setData(data));
}
  render() {
  return (
    <div className="App">
      <input onChange={(event) => this.props.setSearchData(event.target.value)}/>
      <DenseTable newsData={this.props.data}/>
    </div>
  )};
}

const mapStateToProps = state => ({
  data: selectors.getData(state),
});
const mapDispatchToProps = dispatch => ({
  setData: payload => dispatch(actions.setData(payload)),
    setSearchData: payload => dispatch(actions.searchData(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);