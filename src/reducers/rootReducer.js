const initialState = {
    data: [],
    searchData: [],
};

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_DATA': return { ...state, data: action.payload, searchData: action.payload };
        case 'SEARCH_DATA':
            return {
                ...state,
                searchData: state.data.filter(item => item.Title.includes(action.payload)),
            };
        default:
            return state;
    }
};