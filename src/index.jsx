import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import App from './App';
import { rootReducer } from './reducers/rootReducer';
import { Provider } from 'react-redux';

const store = createStore(rootReducer);
window.store = store;
ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
  document.getElementById('root')
);

